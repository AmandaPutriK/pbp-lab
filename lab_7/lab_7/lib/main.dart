import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: "Poppins",
        canvasColor: Color.fromRGBO(232, 247, 252, 1),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Riwayat Swab'),
          backgroundColor: Color.fromRGBO(6, 12, 35, 1),
          
        ),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(children: [
              SizedBox(
                height: 20,
              ),
              // TextField(),
              TextFormField(
                decoration: new InputDecoration(
                  labelText: "Tanggal dilakukan swab",
                  icon: Icon(Icons.event,),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Form tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                
                decoration: new InputDecoration(
                  labelText: "Hasil",
                  icon: Icon(Icons.description,),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Form tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                
                decoration: new InputDecoration(
                  labelText: "CT Value",
                  icon: Icon(Icons.create,),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Form tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    print("Submit berhasill");
                  }
                },
              ),
            ]),
          ),
        ),
      ),
    );
  }
}