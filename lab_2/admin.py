from lab_2.models import Note
from django.contrib import admin
from .models import Note

# Register your models here.
admin.site.register(Note)
