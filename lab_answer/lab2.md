1. Perbedaan antara JSON dan XML:
JSON atau JavaScript Object Notation adalah format file untuk menyimpan dan mentransfer data yang ditulis dalam JavaScript serta dapat dibaca oleh manusia. XML adalah extensible markup language yang digunakan untuk menyimpan dan mengirimkan data. Untuk penyimpanan data, pada JSON data ditulis dalam bentuk key and value serta tidak menggunakan tags atau tanda. Sedangkan pada XML data ditulis dalam bentuk tree structure dan selalu dengan tags atau tanda. Oleh karena itu ukuran dokumen XML relatif lebih besar dibandingkan file JSON.

2. Perbedaan antara HTML dan XML:
- HTML atau Hypertext Markup Language digunakan untuk penyajian data sedangkan XML digunakan untuk mengirimkan data.
- XML merupakan bahasa yang sensitif terhadap besar kecilnya huruf sedangkan HTML tidak.
- XML harus memerlukan closing tags sedangkan HTML tidak selalu memerlukan closing tags.
- XML memiliki tags yang tidak ditentukan sebelumnya sedangkan HTML memiliki tags yang sudah ditentukan, misalnya table, h1, dan sebagainya.